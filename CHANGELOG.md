# Примечания к выпуску
Все заметные изменения в этом проекте будут задокументированы в этом файле. Этот проект придерживается [семантического версионирования](https://semver.org/lang/ru/).

`alfo` - `alfа` на международном языке (илингво, эсперанто)

# 0.10.0-alfo (2020-09-15)

Описание готовится.

# 0.9.0-alfo (2020-08-12)

Описание готовится.

# 0.8.0-alfo (2020-07-08)

## Новый функционал / Features:

- [Сделать базовый функционал уничтожения объектов](https://gitlab.com/tehnokom/universo/-/issues/56)
- [Создать вариант окна с таймером до возможности закрытия](https://gitlab.com/tehnokom/universo/-/issues/83)
- [Сделать пункт меню и окно Рекламная информация](https://gitlab.com/tehnokom/universo/-/issues/84) 
- [Сделать бызовый функционал выхода из приложения и кнопку Выход](https://gitlab.com/tehnokom/universo/-/issues/85)
- [Сделать возможность раскрытия Клиентского приложения на весь экран](https://gitlab.com/tehnokom/universo/-/issues/111)
- [Движение чужих объектов в реальном времени](https://gitlab.com/tehnokom/universo/-/issues/117)
- [Реализовать функционал стрельбы лазером через проекты / задачи](https://gitlab.com/tehnokom/universo/-/issues/118)

## Улучшения / Improvements:

- [Удалить тестовый блок toroid и тестовый функционал на GDNative C++](https://gitlab.com/tehnokom/universo/-/issues/95)

## Исправление ошибок / Bug fixes:

- [Корабль появляется в неправильном месте и нет списка объектов](https://gitlab.com/tehnokom/universo/-/issues/120)

# 0.7.0-alfo (2020-06-06)

## Новый функционал / Features:

- [Добавить базовые шаблоны для нового пользователя в Просперо](https://gitlab.com/tehnokom/universo/-/issues/68)
- [Создать базовые шаблоны проектов / задач пользователя для объединения в основные организации](https://gitlab.com/tehnokom/universo/-/issues/94)
- [Добавить новые кнопки в верхнее меню](https://gitlab.com/tehnokom/universo/-/issues/80)
- [Реализовать функционал соединения через WebSocket](https://gitlab.com/tehnokom/universo/-/issues/74)
- [Изучить возможность открытия клиентского приложения Универсо по умолчанию на весь экран](https://gitlab.com/tehnokom/universo/-/issues/18)
- [Сделать функционал стрельбы лазером](https://gitlab.com/tehnokom/universo/-/issues/53)
- [Проработать функционал модификаций ресурсов](https://gitlab.com/tehnokom/universo/-/issues/75)
- [Сделать функционал астероидов в Универсо](https://gitlab.com/tehnokom/universo/-/issues/51)

## Улучшения / Improvements:

- [Доработать систему меню](https://gitlab.com/tehnokom/universo/-/issues/32)
- [Убрать временные шрифты у некоторых блоков](https://gitlab.com/tehnokom/universo/-/issues/69)
- [Сделать курсор-палец при наведении на вкладки и кнопки](https://gitlab.com/tehnokom/universo/-/issues/81)
- [Проработать архитектуру функционала окон](https://gitlab.com/tehnokom/universo/-/issues/86)


# 0.6.0-alfo (2020-05-08)

## Критические изменения / Breaking changes:

+ Значительный рефакторинг блока космоса.
+ Рефакторинг базовый логики работы в виртуальных параллельных мирах, убрано тестовое содержимое и тестовые связи, удалены старые файлы.
+ Переименованные некоторые блоки.

## Новый функционал / Features:

+ Реализована связь объектов с бэкендом, объекты выводятся на основе данных бэкенда.
+ Реализован полёт объектов через функционал специальных проектов / задач движения.
+ Сделана панель объектов.
+ Реализован базовый функционал движения по маршруту.
+ Реализован функционал определения расстояний до объектов.
+ Реализован функционал подлёта к цели.
+ У пользователей в бэкенде реализован функционал шаблонов по которым создаётся перечень сущностей для нового пользователя, клиентское приложение посылает нужный сигнал.
+ Реализовано появление корабля при первом входе внутри базовой станции.
+ Реализован вылет со станции и залёт на станцию.
+ Повышена плавность появления корабля в космосе при переключении вида из станции на космос.
+ Реализована базовая работа всего существующего функционала двух виртуальных параллельных мирах.

## Улучшения / Improvements:

+ Начато внедрение основного рабочего дизайна системы меню, вместо тестового.
+ Заменены тестовые модели корабля и станции на базовые рабочие модели.
+ Изменён функционал управления камерой внутри ангара станции.
+ Первичная локация начала перемещаться дальше от звезды.
+ Доработана работа с никнеймом под новый формат бэкенда.
+ Базовый шрифт заменён на более читаемый.
+ Доработаны некоторые тексты и надписи.

# 0.5.0-alfo (2020-04-01)

## Новый функционал / Features:

+ Создан виртуальный космос с базовым функционалом, в том числе возможностью полёта тестовым космическим кораблём с управлением мышкой.
+ Создан базовый ангар космической станции с возможностью управления в нём тестовым юнитом.

# 0.4.0-alfo (2020-03-02)

## Критические изменения / Breaking changes:

+ Проект разделён на клиентское приложение и тестовое серверное приложение.
+ Значительный рефакторинг структуры проекта.

## Новый функционал / Features:

+ Сделано новое Главное меню.
+ Главное меню перенесено в ядро проекта.
+ Сделана главная сцена в виде космической станции, для начала помещена временная картинка.
+ Вынесен в отдельный блок функционал авторизации.
+ Вынесен в отдельный блок функционал ресурсного центра.
+ Создан многооконный интерфейс.

# 0.3.0-alfo (2020-02-06)

## Критические изменения / Breaking changes:

+ Дополнительно изменена структура проекта, несколько отдельных проектов Godot объединены в один.

## Новый функционал / Features:

+ Сделана сцена авторизации и связь по API с бэкендом 
+ Сделано многоязычие на сцене авторизации, переключение между 3 языками: Русский, Английский и Международный.
+ Добавлен тестовый код получения данных в Ресурсный центр по API из бэкенда
+ Добавлено Главное меню

# 0.2.0-alfo (2019-12-20)

## Критические изменения / Breaking changes:

+ Полностью изменена структура проекта, начато внедрение блочной структуры

## Новый функционал / Features:

+ Добавлено описание проекта

# 0.1.0-alfo (2019-12-09)

## Критические изменения / Breaking changes:

+ Проект создан

## Новый функционал / Features:

+ Создание 3D сцены с тором и объектом "игрок" в виде параллелепипеда с управлением с клавиатуры
