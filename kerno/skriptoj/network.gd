extends Node

# WebSocket - клиент
class_name GraphQLWSConnector


# Signal to let GUI know whats up
signal connection_failed()
signal connection_succeeded()
signal server_disconnected()
signal input_data()


# настройки по справочнику
# константы соответствия категорий, статусов и т.п.
# категории задач:
const kategorio_movado = 3 # категория движения объекта
const tasko_kategorio_celilo = 6 # категория задачи прицеливания
const tasko_kategorio_pafo = 7 # категория задачи выстрела
const projekto_kategorio_pafado = 6 # категория проекта стрельбы (включает задачи прицеливания и выстрела)
const projekto_tipo_objekto = 2 # тип проектов "Для объектов"
const tasko_tipo_objekto = 2 # тип задач "Для объектов"
const statuso_nova = 1 # статус "Новый"
const statuso_laboranta = 2 # статус "В работе"
const statuso_fermo = 4 # статус "Закрыт"
const status_pauzo = 6 # статус "Приостановлен"
const statuso_posedanto = 1 # статус владельца "Активный"
const tipo_posedanto = 1 # тип владения "Полное владение"


# пришедшие данные с сервера
var data_server = []


# запросы отправляются с очередным id 
var current_query_id = 1

var net_id_clear = [] # список пустых id запросов (ответы не анализируются, а удаляются)


export var websocket_url = "wss://t34.tehnokom.su/api/v1.1/ws/"
var _client = WebSocketClient.new()
var connected = false



# Хэш со всеми игроками зарегистрированными на сервере {id: name}
# заполняется функцией register_player()
var players = {}




func _ready():
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_error")
	_client.connect("connection_established", self, "_connected")
	_client.connect("data_received", self, "_on_data")


func _closed(was_clean = false):
	# was_clean will tell you if the disconnection was correctly notified
	# by the remote peer before closing the socket.
	print("Closed, clean: ", was_clean)
	connected = false
	# set_process(false)
	emit_signal("server_disconnected")
	connect_to_server()


func _error(was_clean = false):
	print("Closed, error: ", was_clean)
	connected = false
	emit_signal("connection_failed")
	connect_to_server()


func _connected(proto = ""):
	print("Connected with protocol: ", proto)
	# Вот тут отправляем инициирущий пакет
	_client.get_peer(1).put_packet(JSON.print({
			'type': 'connection_init',
			'payload': {}
	}).to_utf8())


# получение данных с сервера
func _on_data():
	# Print the received packet, you MUST always use get_peer(1).get_packet
	# to receive data from server, and not get_packet directly when not
	# using the MultiplayerAPI.
	var jdata = _client.get_peer(1).get_packet().get_string_from_utf8()
	var data = JSON.parse(jdata).result
	
	if data.has('type') && data['type'] == 'connection_ack':
		connected = true
		emit_signal("connection_succeeded")
	if data.has('type') && data['type'] == 'data':
		# print('===data= ',data)
		# проверяем, нужно ли обрабатывать данный id
		var index_net = net_id_clear.find(int(data['id']))
		if index_net > -1: # находится в списке очищаемых запросов
			net_id_clear.remove(index_net)
		else:
			data_server.append(data)
			emit_signal("input_data")
	# print("Got data from server: ", jdata)
	# print('emit_signal("input_data")')


func _process(_delta):
	# Что бы получать данные с сервера
	_client.poll()


func _exit_tree():
	_client.disconnect_from_host()


# отправка данных на сервер
func send_data(data, id=0): # если id==0 то используем current_query_id
	if !id:
		id = current_query_id
		current_query_id += 1
	var query = JSON.print({
		'type': 'start',
		'id': '%s' % id,
		'payload': data
	})
	_client.get_peer(1).put_packet(query.to_utf8())
	return id


# отправка данных на сервер
func send_json(data):
	_client.get_peer(1).put_packet(data.to_utf8())


func connect_to_server():
	# print('===input WS')
	var protocols2 = PoolStringArray(["graphql-ws"])
	var err = _client.connect_to_url(websocket_url, protocols2, false, Global.backend_headers)
	if err != OK:
		print("Unable to connect = ", err)
	else:
		_client.get_peer(1).set_write_mode(WebSocketPeer.WRITE_MODE_TEXT)
		
	return err


# передаёт корректный очередной номер и увеличивает его для следующей передачи
func get_current_query_id():
	var id = current_query_id
	current_query_id += 1
	return id


