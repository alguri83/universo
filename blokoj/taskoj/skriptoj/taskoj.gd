extends "res://kerno/fenestroj/tipo_a1.gd"

const QueryObject = preload("queries.gd")

var ItemListContent = []
var id_projekto = []
var id_taskoj = []


func _ready():
	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('Ошибка подключения связи с обменом данных = ',err)


func add_list(on_data):
	for item in on_data:
		ItemListContent.append(item['node']['nomo']['enhavo'])
	FillItemList()


func load_list(on_data):
	add_list(on_data)
	FillItemList()


func clear_list():
	ItemListContent.clear()
	$VBox/body_texture/ItemList.clear()


func _on_data():
	var i_data_server = 0
#	var q = QueryObject.new()
	for on_data in Net.data_server:
#		var index = id_projekto_direkt_del.find(int(on_data['id']))
		var index_projekto = id_projekto.find(int(on_data['id']))
		var index_taskoj = id_taskoj.find(int(on_data['id']))
		if index_projekto > -1: # находится в списке отправленных запросов
			add_list(on_data['payload']['data']['universoProjekto']['edges'])
			if on_data['payload']['data']['universoProjekto']['pageInfo']['hasNextPage']:
				mendo_informoj_projekto(on_data['payload']['data']['universoProjekto']['pageInfo']['endCursor'])
			# удаляем из списка
			id_projekto.remove(index_projekto)
			Net.data_server.remove(i_data_server)
		elif index_taskoj > -1: # находится в списке удаляемых объектов
			add_list(on_data['payload']['data']['universoTasko']['edges'])
			if on_data['payload']['data']['universoTasko']['pageInfo']['hasNextPage']:
				mendo_informoj_tasko(on_data['payload']['data']['universoTasko']['pageInfo']['endCursor'])
			# удаляем из списка
			id_projekto.remove(index_projekto)
			Net.data_server.remove(i_data_server)
		i_data_server += 1

# перезагружаем список объектов
func _reload_taskoj():
	$"VBox/body_texture/ItemList".clear()
	FillItemList()


func FillItemList():
	# Заполняет список найдеными продуктами
	for Item in ItemListContent:
		get_node("VBox/body_texture/ItemList").add_item(Item, null, true)


# Загружаем данные задач
func mendo_informoj_tasko(after=""): # заказ сведений
	if !after:
		clear_list()
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
	id_taskoj.push_back(id)
	# Делаем запрос к бэкэнду
	Net.send_json(q.taskoj_query_ws(id, after))


# Загружаем данные проектов
func mendo_informoj_projekto(after=""): # заказ сведений
	if !after:
		clear_list()
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
	id_projekto.push_back(id)
	# Делаем запрос к бэкэнду
	Net.send_json(q.taskoj_projekto_ws(id, after))




func _on_ItemList_item_activated(index):
	if ($VBox/HBox_top/top_texture2/HBox_buttons/menuo_name.text != "Задачи"):
		$VBox/HBox_top/top_texture2/HBox_buttons/menuo_name.text = "Задачи"
		mendo_informoj_tasko()
