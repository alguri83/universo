extends Spatial

# общий скрипт для всех модулей

var uuid = ""
var integreco = 100
var potenco
var objekto_modulo


# изменить данные в objekto_modulo
func sxangxi_objekto(item):
	for obj in objekto_modulo['ligilo']['edges']:
		#ищем ресурс в переменной
		if obj['node']['ligilo']['uuid'] == item['objekto']['uuid']:
			# вносим изменения по данному объекту
			obj['node']['ligilo']['integreco'] = item['objekto']['integreco']
			obj['node']['ligilo']['volumenoInterna'] = item['objekto']['volumenoInterna']
			obj['node']['ligilo']['volumenoEkstera'] = item['objekto']['volumenoEkstera']
			obj['node']['ligilo']['volumenoStokado'] = item['objekto']['volumenoStokado']


# добавляем ресурс в objekto_modulo
func krei_objekto(item):
	# если нет такого объекта, то добавляем его
	objekto_modulo['ligilo']['edges'].push_back({
		'node': {
			'uuid': item['objekto']['ligiloLigilo']['edges'].front()['node']['uuid'],
			'tipo': item['objekto']['ligiloLigilo']['edges'].front()['node']['tipo'].duplicate(true),
			'ligilo': {
				'uuid': item['objekto']['uuid'],
				'integreco': item['objekto']['integreco'],
				'nomo': {'enhavo': item['objekto']['nomo']['enhavo']},
				'volumenoInterna': item['objekto']['volumenoInterna'],
				'volumenoEkstera': item['objekto']['volumenoEkstera'],
				'volumenoStokado': item['objekto']['volumenoStokado'],
				'resurso': {'objId': item['objekto']['resurso']['objId']}
			}
		}
	})
